let map;
let myArray = [];

if('geolocation' in navigator) {
  navigator.geolocation.getCurrentPosition(success, error)
} else {
  error()
}

function success (location){
    //alert("Geolocation available")
    mapboxgl.accessToken = 'pk.eyJ1IjoidGVhbTA1IiwiYSI6ImNrbzJmZ3M5MDAxZGwydnF3NDN3ZG9paHEifQ.HFVr4kdzLuJ5xGtIhha3Zg';
        map = new mapboxgl.Map({
        container: 'map', // container id
        style: 'mapbox://styles/mapbox/streets-v11', // style URL
        center: [location.coords.longitude, location.coords.latitude], // starting position [lng, lat]
        zoom: 9 // starting zoom
    });
    
    bounds();
}
 

function bounds(){
    for(let i = 0; i < myArray.length; i++){
        myArray[i].remove()    
    }
    
    myArray = []
    
    //task 3
    let Boundaries = map.getBounds();
    
    
    
    //task4
    let latlng = Boundaries._ne.lat + "," + Boundaries._ne.lng + "," + Boundaries._sw.lat + "," + Boundaries._sw.lng;
    
    
    let QueryString = "https://api.waqi.info/map/bounds/?token=fa76901e7f17b06692852e9997dcfa2bf6fc8408&latlng="+ latlng +"&callback=display"
    
    let script = document.createElement("script");
    script.src = QueryString;
    document.body.appendChild(script)
}

function display(result){
    console.log(result.data[0])
    
    for(let i = 0; i < result.data.length; i++){
        
        myArray.push(new mapboxgl.Marker({
            color: AQItoColour(result.data[i].aqi),
        }).setLngLat([result.data[i].lon, result.data[i].lat])
        .addTo(map));
        
        myArray.push(new mapboxgl.Popup({offset:20})
        .setLngLat([result.data[i].lon, result.data[i].lat])
        .setHTML("<h7>"+ result.data[i].station.name +"<h7>")
        .addTo(map));
    }
    
     map.on('zoomend',function(){
         bounds()
     
     });    
}

function AQItoColour (aqi){
    let colour
    
    if(aqi > 0 && aqi < 50){
        colour = "green"
    }else if(aqi > 51 && aqi < 100){
        colour = "yellow"
    }else if(aqi > 101 && aqi < 150){
        colour = "orange"
    }else if(aqi > 151 && aqi < 200){
        colour = "red"
    }else if(aqi > 201 && aqi < 300){
        colour = "purple"
    }else if(aqi > 301 && aqi < 500){
        colour = "maroon"
    }else{
        colour = "white"
    }
    
    
    return colour
}

function error (){
    alert("Geolocation not available")
}



























