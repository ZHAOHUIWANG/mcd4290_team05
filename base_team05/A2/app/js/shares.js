// Constants used as KEYS for LocalStorage
const TRIP_INDEX_KEY = "viewedTripIndex";
const TRIP_DATA_KEY = "tripLocalData";
const TRIP_SEARCH_KEY="searchTripData"



//class trip, class for single trip
class Trip{
    constructor(date,time,place,num,text,sourceA,destinationA,sourceLocation,destinationLocation){
        this._name=text
        this._date=date
        this._source=sourceA
        this._destination=destinationA
        this._time=time
        this._country=place
        this._stops=num
        this._sLocation=sourceLocation
        this._dLocation=destinationLocation
    }
    get name(){
        return this._name
    }
    get country(){
        return this._country
    }
    get date(){
        return this._date
    }
     get time(){
        return this._time
    }
    get stops(){
        return this._stops
    }
    get source(){
        return this._source
    }
    get destination(){
        return this._destination
    }
    get sLocation(){
        return this._sLocation
    }
      get dLocation(){
        return this._dLocation
    }
    
    set name(text){
        this._name=text
    }
     set date(day){
        this._date=day
    }
    set time(time){
        this._time=time
    }
     set stops(num){
        this._stops=array
    }
    set source(text){
        this._source=text
    }
    set destination(text){
        this._destination=text
    }
    fromData(data){
        this._name=data._name
        this._date=data._date
        this._stops=data._stops
        this._country=data._country
        this._source=data._source
        this._sLocation=data._sLocation
        this._dLocation=data._dLocation
    }
}

//TripList class, for schedueled trips
class TripList{
    constructor(){
        this._trips=[]
    }
    get trips(){
        return this._trips
    }
    get count(){
        return this._trips.length
    }
    addTrip(date,time,place,num,text,long,sourceA,destinationA,sourceLocation,destinationLocation){
        this._trips.push(new Trip(date,time,place,num,text,long,sourceA,destinationA,sourceLocation,destinationLocation))
    }
    getTrip(index){
        return this._trips[index]
    }
    cancelTrip(Name){
        for(let i=0;i<TripList.count();i++){
            if (TripList.getTrip(i).name()==Name){
                TripList.splice(i,1)
                break
            }
        }
    }
    fromData(data){
        this._trips=data._trips
    }
}
// TODO: Write the function updateLocalStorage
function updateLocalStorage(data){
    localStorage.setItem(TRIP_DATA_KEY,JSON.stringify(data))
}

// TODO: Write the function getDataLocalStorage
function getDataLocalStorage(){
    return JSON.parse(localStorage.getItem(TRIP_DATA_KEY))
}

// Global LockerList instance variable
let trips = new TripList();

//change to all_trips.html
function backAllTriPage(){
    window.location.href="all_trips.html"; 
}

//change to index.html
function backHomePage(){
    
    window.location.href="index.html"; 
}

//change to detail.html
function goToDetailPage(){
    window.location.href="detail.html"; 
}

//open the confirm windows
document.getElementById("save").addEventListener('click',function(){
    
    document.querySelector('.confirmPage').style.display = 'flex';
});

//close the confirm windows
document.querySelector('.closeButton').addEventListener('click',function(){
    document.querySelector('.confirmPage').style.display = 'none';
});

//close the Detail page
function closeDetailPage(){
    window.location.href="all_trips.html"; 
}

        
      
