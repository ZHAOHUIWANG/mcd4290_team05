//shecule the basic information in main page, this function is to create a new trip in array
/*get the input imformation from the main.html, use this as parameters to create a new trip in triplist*/
//shecule the basic information in main page, this function is to store the information into local storage called TRIP_SEARCH_KEY and change to sche.html
let countryName=document.getElementById("intialcountry");//Let html's element that we firstly choose country
let searchTime=document.getElementById("startDate");//Let html's element that we firstly choose date
let searchDate=document.getElementById("startTime");//Let html's element that we firstly choose time
function checkValue(type) {
                let currentValue = document.getElementById(type).value;
                switch (type) {
                    case "intialcountry": intialcountry = currentValue; break;
                    case "startDate": startDate = currentValue; break;
                    case "startTime": startTime = currentValue; break;
                }
                if (intialcountry && startDate && startTime) {
                    document.getElementById('basicinfo').disabled = '';
                } else {
                    document.getElementById('basicinfo').disabled = 'disabled';
                }
}
function basicinfo_onclick() {
                window.location.href = 'sche.html';
                localStorage.setItem('intialcountry', intialcountry);
                localStorage.setItem('startDate', startDate);
                localStorage.setItem('startTime', startTime);
}
function gotoschepage(){
            window.location.href="sche.html"; 
}

//////////////////////////map/////////////////////////////////
let intialcountry, startDate, startTime;
let map;
let countryNow = localStorage.getItem('intialcountry', intialcountry);//
let dateNow = localStorage.getItem('startDate', startDate);
let timeNow = localStorage.getItem('startTime', startTime);
let airports='';//airport's api's output
let DestinationName=[]//end airport's name
let routesDetail=""//that what we choose aiport's route
let Array_airpotsName = [];//airport's Name
let Array_airpotsLongitude = [];//airport's Longitude
let Array_airpotsLatitude = [];//airport's Latitude

function getAPIData(url, key,callback) {
        let QueryString = url + "?country=" + key + "&callback=" + callback
        let script = document.createElement('script');
        script.src = QueryString
        document.body.appendChild(script);
}//The airports API returns the list of airports in a given country
function getRAPIData(url, key,callback) {
        let QueryString = url + "?sourceAirport=" + key + "&callback=" + callback
        let script = document.createElement('script');
        script.src = QueryString
        document.body.appendChild(script);
}// Get all airport's route from Route Api that what airport we are in. 

let airports_API = "https://eng1003.monash/OpenFlights/airports/?country="+ countryNow +"&callback=makeATrip"
let script = document.createElement('script');
script.src = airports_API;
document.body.appendChild(script);

//show the airpots marker of selected country
function makeATrip(results){
        airports=results;
    console.log(airports)

        Country.innerHTML += countryNow;//Mapbox's infomation
        date.innerHTML += dateNow;//Mapbox's infomation  
        Time.innerHTML += timeNow//Mapbox's infomation 
    
        Country2.innerHTML += countryNow;//Right side of schedule page's infomation
        date2.innerHTML += dateNow;//Right side of schedule page's infomation
        Time2.innerHTML += timeNow;//Right side of schedule page's infomation
        console.log(countryNow)
        mapboxgl.accessToken = 'pk.eyJ1IjoidGVhbTA1IiwiYSI6ImNrbzJmZ3M5MDAxZGwydnF3NDN3ZG9paHEifQ.HFVr4kdzLuJ5xGtIhha3Zg';
            map = new mapboxgl.Map({
            container: 'map', // container id
            style: 'mapbox://styles/mapbox/streets-v11', // style URL
            center: [results[0].longitude,results[0].latitude], // starting position [lng, lat]
            zoom: 3, // starting zoom
            trackResize: true,
        });

        for(let i = 0; i < results.length; i++){
            
            Array_airpotsName.push(results[i].name);
            
            Array_airpotsLongitude.push(results[i].longitude);
            
            Array_airpotsLatitude.push(results[i].latitude);
            
            new mapboxgl.Marker({
                color: "black",
            }).setLngLat([results[i].longitude,results[i].latitude])
            .setPopup(new mapboxgl.Popup({offset:5}).setHTML("<h7>"+ results[i].name +"<h7>")) 
            .addTo(map)
             new mapboxgl.Popup({offset:5})
            .setLngLat([results[i].longitude,results[i].latitude])
            .setHTML("<h7>"+ results[i].name +"<h7>")
            .addTo(map);
        }//map's marker that what we click a point in map
  
            addAirpots("start-airport", airports)//the intialpoint of line in map

        
}

function getFinalAirports(results){
    routesDetail=results
    console.log(routesDetail)
    console.log(airports)
    for(let i=0;i<airports.length;i++){
        for(let j=0;j<routesDetail.length;j++){ 
            if(airports[i]["IATA-FAA"]!==routesDetail[j]["destinationAirport"]){
      
  }
            else{
      DestinationName.push(airports[i].name);
      console.log(DestinationName) ;
     addAirpots11("end-airport",DestinationName);
  }
        }
    }
}//two forloop to find airport api's name and what we choose intial airport's can connect's airport's name, then we use if to find which end airport's position and add in end airport's option's list.
function showAirports() {
    let start = document.getElementById("start-airport").value;
    let sourceID=airports.find(obj=>obj.name==start)["airportId"]
    console.log(sourceID)

    let url="https://eng1003.monash/OpenFlights/routes"
    let callback="getFinalAirports"
  getRAPIData(url, sourceID,callback)
    }//Get route's api's infomation to have all airport routes.
function addAirpots(id, data) {
data.map(function (item) {
    let div = document.createElement('option');
    div.innerHTML = item.name;
    document.getElementById(id).appendChild(div);
})
}//get intialpoint of line of intialairport.
function addAirpots11(id, data) {
data.map(function (item) {
    let div = document.createElement('option');
    div.innerHTML = item;
    document.getElementById(id).appendChild(div);
})
}//get end point of line of desitionation airport.
function DeleteEndAirport(){
    
    document.getElementById("end-airport").options.length=0;
}//clear list of endairport's list in html
function searchRoutes() {
      //alert("it is work!");
    let start = document.getElementById("start-airport").value;
    let end = document.getElementById("end-airport").value;
    let longitude_start = '';
    let latitude_start = '';   
    let longitude_end = ''; 
    let latitude_end = '';
    airport1.innerHTML ='IntialAirport: '+ start;
    airport2.innerHTML ='FinalAirport: ' +end;
    console.log(start)
    console.log(end)
    for(let i = 0; i < Array_airpotsName.length; i++){
        if(Array_airpotsName[i] == start){
            
            longitude_start = Array_airpotsLongitude[i];
            latitude_start = Array_airpotsLatitude[i];
            
            console.log([longitude_start,latitude_start])
           }
        if(Array_airpotsName[i] == end){
            
            longitude_end = Array_airpotsLongitude[i];
            latitude_end = Array_airpotsLatitude[i];
            
            console.log([longitude_end,latitude_end])
           }
    }
    showRoute([longitude_start,latitude_start ], [longitude_end, latitude_end])
    }//when we get start and end airports name then we collect infomation from airport api to get position.

function undo(){
   if (map.getLayer("route")) {
            map.removeLayer('route');
            map.removeSource("airpotsOfSelect");
    }
    
}//remove map's line that we want to client cancel what they do in last touch.

//addLayer
function showRoute(start, end) {
    if (map.getLayer("route")) {
            map.removeLayer('route');
            map.removeSource("airpotsOfSelect");
    }

    map.addSource('airpotsOfSelect',{
        'type':"geojson",
        "data": {
            "type": "Feature",
            "properties": {},
            "geometry": {
            "type": "LineString",
            "coordinates": [start, end]
            }
        }  
    });

    map.addLayer({
        "id": "route",
        "source":"airpotsOfSelect",
        "type": "line",

        "layer":{
            "type": "LineString",
        },

        "layout": {
            "line-join": "round",
            "line-cap": "round",
        },
        "paint": {
            "line-color": "red",
            "line-width": 2,
        }
    });
}// map line's infomation output of start and end airports.


function SaveTrips(data){
    let start = document.getElementById("start-airport").value;
    let end = document.getElementById("end-airport").value;
    let source=airports.find(obj=>obj.name==start)
    let destination=airports.find(obj=>obj.name==end)
    let chooseRoute=routesDetail.find(obj=>obj.destinationAirport==destination["IATA-FAA"])
    console.log(source)
    console.log(destination)
    console.log(chooseRoute)
    let sourceLocation={
        Latitude:source.latitude,
        Longitude:source.longitude
    }
    let destinationLocation={
        Latitude:destination.latitude,
        Longitude:destination.longitude
    }
    data.addTrip(dateNow,timeNow,countryNow,chooseRoute.stops,chooseRoute.airline+" "+chooseRoute.airlineId,source.name,destination.name,sourceLocation,destinationLocation)
    updateLocalStorage(data)
    window.location.href="detail.html"
}//create trip class with selected route detial and store into local storage



